* 个性地址，在网页地址栏可见
* 贡献度
* git bash优点：适合开发 -- 类linux，可以使用linux命令。
* git的颗粒度：行；
* 

```
$ ls -a .git/
./   COMMIT_EDITMSG  description（描述）  hooks/(钩子)  info/  objects/(记录对象哈希值)   packed-refs(存refs不要的，比如gc后)
../  config（配置）          HEAD(分支指针文件)         index（暂存区）   logs/（日志）  ORIG_HEAD（暂时存档点）  refs/(三个文件夹存了分支，远程库，标签)
$ ls hooks
applypatch-msg.sample*      pre-applypatch.sample*      pre-rebase.sample*
commit-msg.sample*          pre-commit.sample*          pre-receive.sample*
fsmonitor-watchman.sample*  prepare-commit-msg.sample*  update.sample*
post-update.sample*         pre-push.sample*
windows有限制，比如不能创建如.git-demo等文件，所以建议用bash
hooks->Git 钩子
  和其它版本控制系统一样，Git 能在特定的重要动作发生时触发自定义脚本。  
  钩子被存储在项目中的 .git/hooks 。 当你用 git init 初始化一个新版本库时，Git 默认会在这个目录中放置一些示例脚本。这些脚本除了本身可以被调用外，它们还透露了被触发时所传入的参数。

$ cat ORIG_HEAD
9920a1b4d9dd823ed7f34f1f74017edb24351269（是merged前的版本号）
作用介绍:
  当进行一些有风险的操作的时候，如reset、merge或者rebase，Git会将HEAD原来所指向commit对象的sha-1值存放于ORIG_HEAD文件中。也就是说ORIG_HEAD可以让我们找到进行最近一次危险操作之前的HEAD位置。

$ cat packed-refs
# pack-refs with: peeled fully-peeled sorted

$ ls objects/
01/  1b/  38/  4c/  63/  70/  7b/  8e/  ac/  b9/  c9/  dd/  f5/    pack/
05/  1f/  3f/  56/  6a/  71/  83/  93/  ae/  ba/  d1/  e0/  fa/
0a/  30/  41/  5c/  6b/  73/  85/  99/  af/  c0/  d8/  e4/  ff/
10/  35/  4b/  5f/  6e/  78/  88/  a5/  b0/  c7/  db/  f3/  info/

$ ls logs/
HEAD  refs/
$ ls logs/refs/
heads/  remotes/

$ git config --global --get user.email
jokerm_13@163.com
$ git config --global --get user.name
mdc

$git add . && git commit -m "...."
可以放到一起用
```

* 只记录修改，没变的不储存。
* .gitignore

```
中，
.idea/
此时git无法发现.idea/, git add . 也不会加进去。（创建项目时有模板）
```
