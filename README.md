Git日常使用总结：
=====
* Git是分布式版本控制系统，其特点是：
    * 每个人的电脑上都是一个可以直接独立地进行工作的完整版本库（无需联网），多人协助时相互推送修改即可。
    * 与之相比，集中式版本控制系统中，版本库是集中存放在中央服务器的，每次修改都需要与中央服务器来回传文件（必须联网）。
    * 分布式版本控制系统的安全性高，因为每个人电脑里都有完整的版本库，某一个人的电脑坏掉了不要紧，随便从其他人那里复制一个就可以了，不依赖中央服务器的运行。
    * PS：在实际使用时，由于相互推送需要双方都在线，因此，也需要服务器用来方便“交换”大家的修改，没有它大家也一样干活，只是交换修改不方便而已。<br>

Git操作总结:
-----
* 打开Git bash：
```
git config --global user.name "Your Name"
git config --global user.email "email@example.com"
```
    因为Git是分布式版本控制系统，所以，每个机器都必须自报家门：你的名字和Email地址
    注意git config命令的--global参数，用了这个参数，表示你这台机器上所有的Git仓库都会使用这个配置.

    版本库 ：英文名repository，可以简单理解成一个目录，这个目录里面的所有文件都可以被Git管理起来，每个文件的修改、删除，Git都能跟踪，以便任何时刻都可以追踪历史，或者在将来某个时刻可以“还原”。
* 使用git管理仓库
    * git init命令把这个目录变成Git可以管理的仓库
    * 当前目录下多了一个.git的目录，是Git来跟踪管理版本库的,如果你没有看到.git目录，那是因为这个目录默认是隐藏的，用ls -ah命令就可以看见。

```
pwd
mkdir learngit
cd learngit
git init
```
        PS： windows记得使用notepad++（UTF-8 without BOM & 配置环境变量）


* 添加文件以及提交
    * 编写一个readme.txt(用cat 或者 notepad++查看)
    * git add命令把文件添加到仓库
    * git commit命令，-m后面输入的是本次提交的说明
    * git status命令可以让我们时刻掌握仓库当前的状态

```
$ cd learngit
$ git status
On branch master
nothing to commit, working tree clean
$ notepad++ readme.txt
$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)
        modified:   readme.txt
no changes added to commit (use "git add" and/or "git commit -a")
$ git add readme.txt
$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)
        modified:   readme.txt
$ git commit -m "wrote a readme file"
[master d132647] wrote a readme file
 1 file changed, 1 insertion(+), 1 deletion(-)
$ git diff readme.txt（因为已经提交了，所以结果为空）
$ notepad++ readme.txt
$ git diff readme.txt
diff --git a/readme.txt b/readme.txt
index 013b5bc..ddcea80 100644
--- a/readme.txt
+++ b/readme.txt
@@ -1,2 +1,3 @@
 Git is a distributed version control system.
-Git is free software.
\ No newline at end of file
+Git is free software.
+                                                                       --by mdc
\ No newline at end of file
$ git add readme.txt
$ git commit -m "add my name"
[master 4197c0e] add my name
 1 file changed, 2 insertions(+), 1 deletion(-)
```

```
其实也能这样
git add file1.txt
git add file2.txt file3.txt
git commit -m "add 3 files."
```

* 版本回退以及复原
    * commit相当于存档，我们通过存档跳跃到不同的时间点。

```
$ notepad++ readme.txt
$ git diff readme.txt
diff --git a/readme.txt b/readme.txt
index ddcea80..dbc7934 100644
--- a/readme.txt
+++ b/readme.txt
@@ -1,3 +1,3 @@
 Git is a distributed version control system.
-Git is free software.
+Git is free software distributed under the GPL.
                                                                        --by mdc
\ No newline at end of file
$ git add readme.txt
$ git commit -m "add GPL"
[master afd6434] add GPL
 1 file changed, 1 insertion(+), 1 deletion(-)
$ git log
commit afd64349d4a75bb930790e7beec5ec03d343184b (HEAD -> master)
Author: mdc <jokerm_13@163.com>
Date:   Sun Sep 16 00:34:04 2018 +0800
    add GPL
commit 4197c0e9f1602afac003504f758aeb4967f9a281
Author: mdc <jokerm_13@163.com>
Date:   Sat Sep 15 19:45:48 2018 +0800
    add my name
commit d13264789f70ca91c7d01824f4433009fa885ac9
Author: mdc <jokerm_13@163.com>
Date:   Sat Sep 15 19:38:10 2018 +0800
    wrote a readme file
commit 4c3c5bfaa94643a35eb3cd43063277d97c370643
Author: mdc <jokerm_13@163.com>
Date:   Thu Sep 13 23:34:51 2018 +0800
    wrote a readme file
$ git log --pretty=oneline
afd64349d4a75bb930790e7beec5ec03d343184b (HEAD -> master) add GPL
4197c0e9f1602afac003504f758aeb4967f9a281 add my name
d13264789f70ca91c7d01824f4433009fa885ac9 wrote a readme file
4c3c5bfaa94643a35eb3cd43063277d97c370643 wrote a readme file
$ git reset --hard head^^
HEAD is now at d132647 wrote a readme file
$ git log
commit d13264789f70ca91c7d01824f4433009fa885ac9 (HEAD -> master)
Author: mdc <jokerm_13@163.com>
Date:   Sat Sep 15 19:38:10 2018 +0800
    wrote a readme file
commit 4c3c5bfaa94643a35eb3cd43063277d97c370643
Author: mdc <jokerm_13@163.com>
Date:   Thu Sep 13 23:34:51 2018 +0800
    wrote a readme file
$ notepad++ readme.txt
$ git reflog
d132647 (HEAD -> master) HEAD@{0}: reset: moving to head^^
afd6434 HEAD@{1}: commit: add GPL
4197c0e HEAD@{2}: commit: add my name
d132647 (HEAD -> master) HEAD@{3}: commit: wrote a readme file
4c3c5bf HEAD@{4}: commit (initial): wrote a readme file
$ git reset --hard afd6
HEAD is now at afd6434 add GPL
$ notepad++ readme.txt

```
* 暂存区stage原理解释：
![输入图片说明](https://images.gitee.com/uploads/images/2018/0916/004617_4c315d6f_2162412.jpeg "stage暂存区.jpg")    
    * 工作区，目录，即文件夹
    * 版本库，即工作区下的隐藏目录.git
    * stage(index), 暂存区
    * master，git自动创建的第一个分支，以及指向master的一个指针叫HEAD。
* 过程总结:
    * git add实际上就是把文件修改添加到暂存区。
    * git commit实际上就是把暂存区的所有内容提交到当前分支。
    * 即需要提交的文件修改通通放到暂存区，然后，一次性提交暂存区的所有修改。
    * 当提交完后且没有对工作区进行修改时，工作区就是干净的。（暂存区也为空）


* Git跟踪并管理的是 **修改** ，而非文件。
    * 每次修改，如果不用git add到暂存区，那就不会加入到commit中， **因为没有放入暂存区** 。
    * 例子：第一次修改 -> git add -> 第二次修改 -> git commit
    * 提交后，可以用git diff HEAD -- readme.txt查看工作区和版本库里面最新版本的区别

```
$ git reset --hard head
HEAD is now at afd6434 add GPL

$ cat readme.txt
Git is a distributed version control system.
Git is free software distributed under the GPL.
                                                                        --by mdc
$ notepad++ readme.txt
$ git diff readme.txt
diff --git a/readme.txt b/readme.txt
index dbc7934..ffc3b72 100644
--- a/readme.txt
+++ b/readme.txt
@@ -1,3 +1,5 @@
 Git is a distributed version control system.
 Git is free software distributed under the GPL.
+Git has a mutable index called stage.
+Git tracks changes.
                                                                        --by mdc
\ No newline at end of file

$ git add readme.txt
$ notepad++ readme.txt
$ git diff readme.txt
diff --git a/readme.txt b/readme.txt
index ffc3b72..af07f1e 100644
--- a/readme.txt
+++ b/readme.txt
@@ -1,5 +1,5 @@
 Git is a distributed version control system.
 Git is free software distributed under the GPL.
 Git has a mutable index called stage.
-Git tracks changes.
+Git tracks changes of files.
                                                                        --by mdc
\ No newline at end of file

$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)
        modified:   readme.txt
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)
        modified:   readme.txt

$ git commit -m "git tracks changes"
[master e4ec3fc] git tracks changes
 1 file changed, 2 insertions(+)

$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)
        modified:   readme.txt
no changes added to commit (use "git add" and/or "git commit -a")

$ git diff HEAD -- readme.txt
diff --git a/readme.txt b/readme.txt
index ffc3b72..af07f1e 100644
--- a/readme.txt
+++ b/readme.txt
@@ -1,5 +1,5 @@
 Git is a distributed version control system.
 Git is free software distributed under the GPL.
 Git has a mutable index called stage.
-Git tracks changes.
+Git tracks changes of files.
                                                                        --by mdc
\ No newline at end of file

$ git reset --hard head
HEAD is now at e4ec3fc git tracks changes

$ cat readme.txt
Git is a distributed version control system.
Git is free software distributed under the GPL.
Git has a mutable index called stage.
Git tracks changes.
                      
```

* 撤销修改
    * git checkout -- file可以丢弃工作区的修改，用版本库里的版本替换工作区的版本
        * 即让这个文件 **回到最近一次git commit或git add时** 的状态。

        * PS：git checkout -- file命令中的--很重要，没有--，就变成了“切换到另一个分支”的命令
    * git reset HEAD <file>可以把暂存区的修改撤销掉（unstage），重新放回工作区(回退到最新的版本)
* 删除文件
    * 在Git中，删除也是一个修改操作
        * 例子：rm test.txt<br>
    此时，有两种情况，<br>
    1.确实要从版本库中删除该文件，那就用命令git rm删掉，并且git commit<br>
    2.删错了，使用git checkout -- test.txt或者版本回退。
* 删除以及撤销的实例：

```
$ rm readme.txt
$ ls
$ git checkout -- readme.txt
$ ls
readme.txt
$ rm readme.txt
$ git rm test.txt
fatal: pathspec 'test.txt' did not match any files
$ git rm readme.txt
rm 'readme.txt'
$ ls
$ git reset --hard head
HEAD is now at e4ec3fc git tracks changes
$ ls
readme.txt

$ notepad++ test.txt
$ rm test.txt
$ git rm test.txt
fatal: pathspec 'test.txt' did not match any files（因为没有add到master分支中，也就不存在rm）

$ git status
On branch master
nothing to commit, working tree clean
```

* 远程仓库：
    * 即一个24小时待机的服务器，每个人从这个“服务器”仓库克隆一份到自己的电脑上，并把各自的提交推送到服务器仓库里，也从服务器仓库中拉取别人的提交。
    * 用码云实现远程仓库：
        * GitHub(or 码云)需要SSH Key, 因为它们需要公钥识别出你的身份，且Git支持SSH协议。（传输是通过SSH加密的）
        * 1. 创建SSH Key。<br>
          在用户主目录下，看看有没有.ssh目录，如果有，再看看这个目录下有没有id_rsa和id_rsa.pub这两个文件，如果已经有了，可直接跳到下一步。如果没有，打开Shell（Windows下打开Git Bash），创建SSH Key：<br>
        `$ ssh-keygen -t rsa -C "youremail@example.com"`<br>
        然后一路回车，使用默认值即可。(由于这个Key也不是用于军事目的，所以也无需设置密码)

        * 注意：id_rsa是私钥，不能泄露出去，id_rsa.pub是公钥，可以放心地告诉任何人。
        * 2. 设置-> SSH公钥-> 添加公钥（填上任意Title，在Key文本框里粘贴id_rsa.pub文件的内容-> 确定添加
    * 添加远程库
        * 1. 先在码云上创建一个项目learngit
        * 2. 通过使用命令git remote add把它和码云的远程库关联
        * PS: origin，是Git默认的远程库叫法
        * 3. 用git push命令，把当前分支master推送到远程。
        * 注意：由于远程库是空的，我们第一次推送master分支时，加上了-u参数，Git不但会把本地的master分支内容推送的远程新的master分支，还会把本地的master分支和远程的master分支关联起来，在以后的推送或者拉取时就可以简化命令。
        * 4. 之后用`git push origin master`把本地master分支的最新修改推送至GitHub
        * 5.  **出现问题时** ，用`git remote -v`查看远程库信息
        * 6. 用`git remote rm origin`删除已有的远程库
        * 错误原因：
        * 弄错了地址！！！在个人资料中看 `https://gitee.com/mdc233/learngit.git`

```
$ git remote rm origin
$ git remote -v
$ git remote add origin https://gitee.com/mdc233/learngit.git
$ git push -u origin master
Counting objects: 15, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (10/10), done.
Writing objects: 100% (15/15), 1.26 KiB | 258.00 KiB/s, done.
Total 15 (delta 2), reused 0 (delta 0)
remote: Powered by Gitee.com
To https://gitee.com/mdc233/learngit.git
 * [new branch]      master -> master
Branch 'master' set up to track remo
然后发现已经推送至码云上
PS： 把git remote add中的origin换成gitee或github则可用同步多个远程库。（push时同理）
```

* 从远程库克隆

```
$ git clone https://gitee.com/mdc233/git_notes.git
Cloning into 'git_notes'...
remote: Enumerating objects: 84, done.
remote: Counting objects: 100% (84/84), done.
remote: Compressing objects: 100% (77/77), done.
remote: Total 84 (delta 25), reused 0 (delta 0)
Unpacking objects: 100% (84/84), done.

$ ls
git_notes/  readme.txt
$ cd git_notes
$ ls
README.md  windows配置环境变量.md

PS：默认的git://使用ssh，但也可以使用https等其他协议。
```

* 分支管理：
    * 一个分支如同一条时间线，而多个分支则是不同的世界线（做出了不同的选择）
    * 原理图

    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0917/103809_4d8e6e26_2162412.png "分支原理图.png")

    * 如图所示，
        * HEAD指针指向master，master始终指向主分支的最新提交。
        * HEAD表示当前控制的分支，用`git checkout 分支名`命令来选择。
        * `git branch 分支名`命令用于创建新的分支。
            * `git checkout -b`表示创建分支的同时切换到该分支。<br>
            `git branch`直接使用时可以查看分支
        * `git merge`命令用于合并指定分支到当前分支(默认Fast-forward，即一条线直接覆盖上去)
        * `git branch -d 分支名`用于删除分支

```
$ git branch dev
$ git branch
  dev
* master
$ git checkout dev
Switched to branch 'dev'
$ git branch
* dev
  master
$ git branch -d dev
error: Cannot delete branch 'dev' checked out at 'C:/Users/wwwmi/learngit'
(不能删除当前控制分支)

$ git checkout master
Switched to branch 'master'
Your branch is up to date with 'origin/master'.

$ git branch -d dev
Deleted branch dev (was e4ec3fc).
$ git branch
* master

$ git checkout -b dev
Switched to a new branch 'dev'
$ notepad++ readme.txt
$ git diff readme.txt
diff --git a/readme.txt b/readme.txt
index ffc3b72..5cbcb33 100644
--- a/readme.txt
+++ b/readme.txt
@@ -2,4 +2,5 @@ Git is a distributed version control system.
 Git is free software distributed under the GPL.
 Git has a mutable index called stage.
 Git tracks changes.
+Creating a new branch is quick.
                                                                        --by mdc
\ No newline at end of file

$ git add readme.txt
$ git commit -m "branch.test"
[dev 5671349] branch.test
 1 file changed, 1 insertion(+)
$ git checkout master
Switched to branch 'master'
Your branch is up to date with 'origin/master'.

$ git log
commit e4ec3fc950e95f075a4ce470a68ee95536c7d99a (HEAD -> master, origin/master)
Author: mdc <jokerm_13@163.com>
Date:   Sun Sep 16 09:40:09 2018 +0800

    git tracks changes
...(省略)

$ git merge dev
Updating e4ec3fc..5671349
Fast-forward
 readme.txt | 1 +
 1 file changed, 1 insertion(+)

$ git log
commit 56713499a496e96089a2b909d9b6fe97e47edf2b (HEAD -> master, dev)
Author: mdc <jokerm_13@163.com>
Date:   Mon Sep 17 10:52:27 2018 +0800

    branch.test

commit e4ec3fc950e95f075a4ce470a68ee95536c7d99a (origin/master)
Author: mdc <jokerm_13@163.com>
Date:   Sun Sep 16 09:40:09 2018 +0800

    git tracks changes
...（省略）

$ git branch -d dev
Deleted branch dev (was 5671349).
$ git branch
* master
```

* 解决分支冲突
    * 当Git无法自动合并分支时，就必须首先解决冲突。
    * 解决冲突就是把Git合并失败的文件手动编辑为我们希望的内容，再提交。
    * 用`git log --graph( --pretty=oneline --abbrev-commit)`命令可以看到分支合并图。
    * 冲突原理图如下，需要让两条世界线交汇：

![输入图片说明](https://images.gitee.com/uploads/images/2018/0917/110451_2bda997e_2162412.png "冲突原理图.png")


```
$ cd learngit
$ git checkout -b feature1
Switched to a new branch 'feature1'
$ notepad++ readme.txt
$ git diff readme.txt
diff --git a/readme.txt b/readme.txt
index 5cbcb33..88470f8 100644
--- a/readme.txt
+++ b/readme.txt
@@ -3,4 +3,5 @@ Git is free software distributed under the GPL.
 Git has a mutable index called stage.
 Git tracks changes.
 Creating a new branch is quick.
+in feature1
                                                                        --by mdc
\ No newline at end of file

$ git add readme.txt
$ git commit -m "in feature1"
[feature1 303ca56] in feature1
 1 file changed, 1 insertion(+)

$ git checkout master
Switched to branch 'master'
Your branch is up to date with 'origin/master'.
$ notepad++ readme.txt
$ git add readme.txt
$ git commit -m "in master"
[master f3fe38b] in master
 1 file changed, 1 insertion(+)

$ git merge feature1
Auto-merging readme.txt
CONFLICT (content): Merge conflict in readme.txt
Automatic merge failed; fix conflicts and then commit the result.

$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)

Unmerged paths:
  (use "git add <file>..." to mark resolution)

        both modified:   readme.txt

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        git_notes/

no changes added to commit (use "git add" and/or "git commit -a")

$ notepad++ readme.txt
把    <<<<<<< HEAD
      in master
      =======
      in feature1
      >>>>>>> feature1  修改成想要的合并文本
$ git add readme.txt
$ git commit -m "merged"
[master 01310fb] merged

$ git log --graph --pretty=oneline --abbrev-commit
*   01310fb (HEAD -> master) merged
|\
| * 303ca56 (feature1) in feature1
* | f3fe38b in master
|/
* 5671349 (origin/master) branch.test
* e4ec3fc git tracks changes
* afd6434 add GPL
* 4197c0e add my name
* d132647 wrote a readme file
* 4c3c5bf wrote a readme file

$ git branch -d feature1
Deleted branch feature1 (was 303ca56).

```
* 普通（留下痕迹）的合并

```
$ git checkout -b dev
Switched to a new branch 'dev'
$ notepad++ readme.txt
$ git add readme.txt
$ git commit -m "--no-ff merged"
[dev 8e2482d] --no-ff merged
 1 file changed, 1 insertion(+)

$ git checkout master
Switched to branch 'master'
Your branch is ahead of 'origin/master' by 3 commits.
  (use "git push" to publish your local commits)

$ git merge --no-ff -m "merge with no-ff" dev
Merge made by the 'recursive' strategy.
 readme.txt | 1 +
 1 file changed, 1 insertion(+)

$ git branch -d dev
Deleted branch dev (was 8e2482d).
$ git log --graph --pretty=oneline --abbrev-commit
*   7086c5f (HEAD -> master) merge with no-ff
|\
| * 8e2482d --no-ff merged
|/
*   01310fb merged
|\
| * 303ca56 in feature1
* | f3fe38b in master
|/
* 5671349 (origin/master) branch.test
* e4ec3fc git tracks changes
* afd6434 add GPL
* 4197c0e add my name
* d132647 wrote a readme file
* 4c3c5bf wrote a readme file
```

* 修复bug以及stash
    * 每个bug都可以通过一个新的临时分支来修复，修复后，合并分支，然后将临时分支删除。
    * 要在哪个分支上修复bug，就从该分支创建临时bug分支
    * `git stash`功能，可以把当前工作现场“储藏”起来，等以后恢复现场后继续工作，配套功能如下：
        * `git stash list`查看stash内容
        * `git stash pop`，恢复工作现场的同时把stash内容也删了
        * `git stash apply`恢复工作现场，stash内容并不删除，需要用`git stash drop`来删除。
        *  **最大的错误** 出现在： 不能直接在master上操作，要理清时间线的关系！！！这里也是有时间矛盾的！！！
```
$ git checkout -b dev
Switched to a new branch 'dev'
$ notepad++ readme.txt
$ git diff readme.txt
diff --git a/readme.txt b/readme.txt
index e099cf3..83ef5d1 100644
--- a/readme.txt
+++ b/readme.txt
@@ -5,4 +5,5 @@ Git tracks changes.
 Creating a new branch is quick.
 in the same world line.
 for no fast forword
+after solving the bug.
                                                                        --by mdc
\ No newline at end of file

$ git stash
Saved working directory and index state WIP on dev: 7086c5f merge with no-ff
$ git checkout master
Switched to branch 'master'
Your branch is ahead of 'origin/master' by 5 commits.
  (use "git push" to publish your local commits)
$ git checkout -b issue
Switched to a new branch 'issue'
$ notepad++ readme.txt
$ git diff readme.txt
diff --git a/readme.txt b/readme.txt
index e099cf3..38a33c7 100644
--- a/readme.txt
+++ b/readme.txt
@@ -4,5 +4,5 @@ Git has a mutable index called stage.
 Git tracks changes.
 Creating a new branch is quick.
 in the same world line.
-for no fast forword
+for no fast forword.
                                                                        --by mdc
\ No newline at end of file

$ git add readme.txt
$ git commit -m "bug fix issue"
[issue 63bcd3a] bug fix issue
 1 file changed, 1 insertion(+), 1 deletion(-)

$ git checkout master
Switched to branch 'master'
Your branch is ahead of 'origin/master' by 5 commits.
  (use "git push" to publish your local commits)
$ git merge --no-ff -m "merged bug fix" issue
Merge made by the 'recursive' strategy.
 readme.txt | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)
$ git branch -d issue
Deleted branch issue (was 63bcd3a).

$ git log --graph --pretty=oneline --abbrev-commit
*   9920a1b (HEAD -> master) merged bug fix
|\
| * 63bcd3a bug fix issue
|/
*   7086c5f (dev) merge with no-ff
|\
| * 8e2482d --no-ff merged
|/
*   01310fb merged
|\
| * 303ca56 in feature1
* | f3fe38b in master
|/
* 5671349 (origin/master) branch.test
* e4ec3fc git tracks changes
* afd6434 add GPL
* 4197c0e add my name
* d132647 wrote a readme file
* 4c3c5bf wrote a readme file

$ git checkout dev
Switched to branch 'dev'
$ git status
On branch dev
nothing to commit, working tree clean
$ git stash list
stash@{0}: WIP on dev: 7086c5f merge with no-ff
stash@{1}: WIP on master: 5fa686c merged bug fix 101
stash@{2}: WIP on master: 7086c5f merge with no-ff
$ git stash drop stash@{1}
Dropped stash@{1} (93ab8dfbd83101d925bcc2dfcb9b94774de6b02e)
（git stash drop与git stash apply后面都可以接对应的栈地址stash@{n}）

$ git stash list
stash@{0}: WIP on dev: 7086c5f merge with no-ff
stash@{1}: WIP on master: 7086c5f merge with no-ff
$ git stash drop stash@{1}
Dropped stash@{1} (1b8a16fde1aae8f8c8e580028baf4cf2189872b0)
$ git stash list
stash@{0}: WIP on dev: 7086c5f merge with no-ff

$ git stash apply
On branch dev
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   readme.txt

no changes added to commit (use "git add" and/or "git commit -a")
$ git stash list
stash@{0}: WIP on dev: 7086c5f merge with no-ff
$ git add readme.txt
$ git commit -m "after bug"
[dev 6ed67df] after bug
 1 file changed, 1 insertion(+)

$ git checkout master
Switched to branch 'master'
Your branch is ahead of 'origin/master' by 7 commits.
  (use "git push" to publish your local commits)
$ git merge --no-ff -m "merged after bug" dev
Auto-merging readme.txt
CONFLICT (content): Merge conflict in readme.txt
Automatic merge failed; fix conflicts and then commit the result.
$ notepad++ readme.txt
$ git add readme.txt
$ git commit -m "merged after the bug"
[master 3f54b33] merged after the bug

$ git log --graph --pretty=oneline --abbrev-commit
*   3f54b33 (HEAD -> master) merged after the bug
|\
| * 6ed67df (dev) after bug
* |   9920a1b merged bug fix
|\ \
| |/
|/|
| * 63bcd3a bug fix issue
|/
*   7086c5f merge with no-ff
|\
| * 8e2482d --no-ff merged
|/
*   01310fb merged
|\
| * 303ca56 in feature1
* | f3fe38b in master
|/
* 5671349 (origin/master) branch.test
* e4ec3fc git tracks changes
* afd6434 add GPL
* 4197c0e add my name
* d132647 wrote a readme file
:

git branch -d dev
Deleted branch dev (was 6ed67df).
$ git stash drop
Dropped refs/stash@{0} (0a3fef0c8f65425113e1a6e262d23009d0b9343a)
$ git stash list

```

* 标签管理：
    * tag就是一个标识符，跟某个commit绑在一起。<br>
    (注意：标签总是和某个commit挂钩。如果这个commit既出现在master分支，又出现在dev分支，那么在这两个分支上都可以看到这个标签。)

    * `git tag <tagname> [commit id]`用于新建一个标签，默认对象为HEAD，也可以指定一个commit id；
    * `git tag`可以查看所有标签。
    * `git show <tagname>`查看标签信息
    * `git tag -a <tagname> -m "blablabla..."`可以创建带有说明的标签，用-a指定标签名，-m指定说明文字
    
    * 创建的标签都只存储在本地， **不会自动推送到远程** 。
    * `git tag -d <tagname>`可以删除一个本地标签；
    * `git push origin <tagname>`可以向远程推送一个本地标签；
    * `git push origin --tags`可以向远程推送全部未推送过的本地标签；
    * `git push origin :refs/tags/<tagname>`可以删除一个远程标签。( **要先从本地删除** )


```
$ git tag v1.0
$ git tag
v1.0
$ git log --pretty=oneline --abbrev-commit
3f54b33 (HEAD -> master, tag: v1.0) merged after the bug
6ed67df after bug
9920a1b merged bug fix
63bcd3a bug fix issue
7086c5f merge with no-ff
8e2482d --no-ff merged
01310fb merged
f3fe38b in master
303ca56 in feature1
5671349 (origin/master) branch.test
e4ec3fc git tracks changes
afd6434 add GPL
4197c0e add my name
d132647 wrote a readme file
4c3c5bf wrote a readme file

$ git tag v0.9 6ed67df
$ git tag
v0.9
v1.0
$ git show v1.0
commit 3f54b338f8869b47a74db4f4a49177c315d018e7 (HEAD -> master, tag: v1.0)
Merge: 9920a1b 6ed67df
Author: mdc <jokerm_13@163.com>
Date:   Mon Sep 17 17:03:19 2018 +0800

    merged after the bug

diff --cc readme.txt
index 38a33c7,83ef5d1..ae02360
--- a/readme.txt
+++ b/readme.txt
@@@ -4,5 -4,6 +4,6 @@@ Git has a mutable index called stage
  Git tracks changes.
  Creating a new branch is quick.
  in the same world line.
 -for no fast forword
 +for no fast forword.
+ after solving the bug.
                                                                        --by mdc

$ git tag -a v0.1 -m "version 0.1 released" 4c3c
$ git show v0.1
tag v0.1
Tagger: mdc <jokerm_13@163.com>
Date:   Mon Sep 17 21:18:26 2018 +0800

version 0.1 released

commit 4c3c5bfaa94643a35eb3cd43063277d97c370643 (tag: v0.1)
Author: mdc <jokerm_13@163.com>
Date:   Thu Sep 13 23:34:51 2018 +0800

    wrote a readme file

diff --git a/readme.txt b/readme.txt
new file mode 100644
index 0000000..d8036c1
--- /dev/null
+++ b/readme.txt
@@ -0,0 +1,2 @@
+Git is a version control system.
+Git is free software.
\ No newline at end of file
```

```
$ git log --pretty=oneline --abbrev-commit
3f54b33 (HEAD -> master, tag: v1.0, origin/master) merged after the bug
6ed67df (tag: v0.9) after bug
9920a1b merged bug fix
63bcd3a bug fix issue
7086c5f merge with no-ff
8e2482d --no-ff merged
01310fb merged
f3fe38b in master
303ca56 in feature1
5671349 branch.test
e4ec3fc git tracks changes
afd6434 add GPL
4197c0e add my name
d132647 wrote a readme file
4c3c5bf (tag: v0.1) wrote a readme file

$ git tag -d v0.1
Deleted tag 'v0.1' (was 783b72a)
$ git log --pretty=oneline --abbrev-commit
3f54b33 (HEAD -> master, tag: v1.0, origin/master) merged after the bug
6ed67df (tag: v0.9) after bug
9920a1b merged bug fix
63bcd3a bug fix issue
7086c5f merge with no-ff
8e2482d --no-ff merged
01310fb merged
f3fe38b in master
303ca56 in feature1
5671349 branch.test
e4ec3fc git tracks changes
afd6434 add GPL
4197c0e add my name
d132647 wrote a readme file
4c3c5bf wrote a readme file

$ git push origin v1.0
Total 0 (delta 0), reused 0 (delta 0)
remote: Powered by Gitee.com
To https://gitee.com/mdc233/learngit.git
 * [new tag]         v1.0 -> v1.0

$ git push origin --tags
Total 0 (delta 0), reused 0 (delta 0)
remote: Powered by Gitee.com
To https://gitee.com/mdc233/learngit.git
 * [new tag]         v0.9 -> v0.9

$ git tag -d v0.9
Deleted tag 'v0.9' (was 6ed67df)
$ git push origin :refs/tags/v0.9
remote: Powered by Gitee.com
To https://gitee.com/mdc233/learngit.git
 - [deleted]         v0.9

可以直接登陆码云查看结果，在标签栏中
```
